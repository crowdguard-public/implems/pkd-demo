from django.db import models
from django.contrib.auth.models import User
from django.db.models import Q


class InitPhase:
    START = 0
    ABILITY = 1
    WORKERS = 2
    TREE = 3
    TASKS = 4
    END = 5
    OVER = 6

    order = [0, 1, 2, 3, 4, 5, 6]

    @staticmethod
    def is_before(p1, p2):
        """
        Test if the phase p1 is before the phase p2
        :param p1: phase 1
        :param p2: phase 2
        :return: True if p1 is before p2
        """
        return not InitPhase.is_after(p1, p2) and not InitPhase.is_same(p1, p2)

    @staticmethod
    def is_after(p1, p2):
        """
        Test if p1 is after p2
        :param p1: phase 1
        :param p2: phase 2
        :return: True if p1 is after p2, False if p1 is before or equal p2
        """

        ip1 = None
        ip2 = None

        for i in range(len(InitPhase.order)):
            if InitPhase.order[i] == p1:
                ip1 = i

            if InitPhase.order[i] == p2:
                ip2 = i

            if ip1 is not None and ip2 is not None:
                break

        if ip1 is not None and ip2 is not None:
            return ip1 > ip2
        return False

    @staticmethod
    def is_same(p1, p2):
        """
        Test if p1 is on the same time as p2
        :param p1: phase 1
        :param p2: phase 2
        :return:
        """
        return p1 == p2


def next_init_phase(phase):
    if phase == InitPhase.START:
        return InitPhase.ABILITY
    elif phase == InitPhase.ABILITY:
        return InitPhase.WORKERS
    elif phase == InitPhase.END:
       return InitPhase.OVER
    else:
        return InitPhase.OVER


class Settings(models.Model):
    """
    App settings
    """
    # Initialization state
    init = models.IntegerField(default=InitPhase.START)


class Ability(models.Model):
    """
    An ability present in the system
    """
    # Ability name
    name = models.CharField(max_length=120)
    # Ability description
    descr = models.CharField(max_length=512)

    class Meta:
        ordering = ('id', )


class Worker(models.Model):
    """
    A worker
    """
    # Authentication informations
    #auth = models.ForeignKey(User, on_delete=models.CASCADE)

    # Real (True) / Fake (False) user (manually created)
    real = models.BooleanField()

    # Profile complete (answered all abilities)
    completed = models.BooleanField(default=False)

    class Meta:
        ordering = ('id', )


class WorkerAbility(models.Model):
    """
    Ability detained by an user
    """
    # worker
    worker = models.ForeignKey(Worker, on_delete=models.CASCADE, related_name='abilities')
    # Ability
    ability = models.ForeignKey(Ability, on_delete=models.CASCADE, related_name='workers')
    # Competence level
    level = models.FloatField()

    class Meta:
        unique_together = ('worker', 'ability')
        ordering = ('ability', )


class WorkerTreeNodeVolume(models.Model):
    """
    Dimension of a volume
    """
    # Referring ability
    ability = models.ForeignKey(Ability, on_delete=models.CASCADE)
    # Min
    min = models.FloatField()
    # Max
    max = models.FloatField()

    class Meta:
        unique_together = ('ability', 'min', 'max')
        ordering = ('ability', )


class WorkersTreeNode(models.Model):
    """
    Node of a workers tree
    """

    app_count = models.IntegerField()
    volume = models.ManyToManyField(WorkerTreeNodeVolume)
    peoples = models.ManyToManyField(Worker, related_name="volumes")
    epsilon = models.FloatField()

    z = models.FloatField(default=-1)
    f = models.FloatField(default=-1)
    beta = models.FloatField(default=-1)
    alpha = models.FloatField()

    def real_count(self):
        return self.peoples.count()


class WorkersTree(models.Model):
    """
    Workers tree
    """
    cut = models.ForeignKey(Ability, null=True, on_delete=models.CASCADE)
    node = models.ForeignKey(WorkersTreeNode, on_delete=models.CASCADE, related_name='tree')
    parent = models.ForeignKey('WorkersTree', null=True, on_delete=models.CASCADE, related_name='children')
    # List of floats (as a string representation of a python array)
    table_cut = models.CharField(max_length=64)

    def get_table_cut(self):
        """
        Get table cut, from a string representation to an array of ints
        :return:
        """
        s = self.table_cut[1:-1].replace(' ', '').split(',')
        return [int(x) for x in s]


class WorkersTreeMeta(models.Model):
    """
    Generated tree metadata
    """
    # Metadata

    # The tree is being generated
    generating = models.BooleanField(default=True)
    # Tree generation starting time
    time_start = models.DateTimeField(auto_now_add=True)
    # Tree generation end time
    time_end = models.DateTimeField(auto_now=True)
    # Expected number of nodes
    expected = models.IntegerField()
    # Current number of nodes
    current = models.IntegerField(default=0)

    # Tree fingerprint

    # The epsilon (budget) value
    epsilon = models.FloatField()
    # Max tree height
    prof_max = models.IntegerField()
    # Number of bins in the histogram
    bins = models.IntegerField()
    # EPS radio
    ratio_count = models.FloatField()
    # Attacking coalition size
    coalition = models.IntegerField()

    # Info
    root = models.ForeignKey(WorkersTree, null=True, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('epsilon', 'prof_max', 'bins', 'ratio_count', 'coalition')


class Task(models.Model):
    """
    A task for the workers
    """
    # Task name
    title = models.CharField(max_length=64)
    # Task description
    descr = models.CharField(max_length=1024)
    # Tree where the task belong
    tree = models.ForeignKey(WorkersTreeMeta, on_delete=models.CASCADE, related_name='tasks')
    # Real number of workers in the task area
    real_count = models.IntegerField(default=0)
    # Number of nodes concerned by the task
    nodes = models.IntegerField(default=0)
    # Unperturbed count (PKD without differential privacy)
    unpert_count = models.IntegerField(default=0)
    # Perturbed count (PKD with differential privacy)
    pert_count = models.IntegerField(default=0)


class TaskAbility(models.Model):
    """
    Ability level range required for the task
    """
    # Task
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='abilities')
    # Ability
    ability = models.ForeignKey(Ability, on_delete=models.CASCADE, related_name='tasks')
    # Minimum ability level required
    level_min = models.FloatField()
    # Maximum ability level required
    level_max = models.FloatField()

    class Meta:
        unique_together = ('task', 'ability')


class WorkersCSVFile(models.Model):
    """
    An imported (or exported) workers CSV file
    """
    # True : imported, False : exported
    imported = models.BooleanField(default=True)
    # CSV file
    file = models.FileField()
    # Currently beign imported
    importing = models.BooleanField(default=False)