from django import template

register = template.Library()


@register.filter
def get_range_sliced(value):
    offset = 3
    value = int(value)
    return range(value - offset, value + offset + 1)


@register.filter
def get_range(value):
    return range(value)

@register.filter
def sub(a, b):
    return a - b

@register.filter
def get_toplevel(tree):
    """
    Get all top level trees ids
    :param tree:
    :return:
    """
    print("-----")
    print(tree)
    print(tree.parent)

    s = ''
    while tree is not None:
        if s != '':
            s += ','
        s += '#collapse_' + str(tree.id)
        tree = tree.parent
    print(s)
    return s

