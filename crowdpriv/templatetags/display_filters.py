from django import template

register = template.Library()

@register.simple_tag
def get_area_min(min):
    return min * 100

@register.simple_tag
def get_area_range(min, max):
    w_min = get_area_min(min)
    w_max = max * 100
    return w_max - w_min

@register.simple_tag
def get_area_left(min, max):
    return 100 - (max * 100)