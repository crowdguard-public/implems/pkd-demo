from celery.task import task
import csv

from crowdpriv.models import WorkersTreeMeta, Ability, Worker, WorkerAbility, WorkersCSVFile
from crowdpriv.tree import median, count, eps, dim
from crowdpriv.tree.kdtree import init_node, kdtree


@task(name="task_tree_generation")
def task_tree_generation(pk):
    """
    Generate a new tree
    :param pk: tree metadata public key
    :return:
    """
    tree = WorkersTreeMeta.objects.get(pk=pk)
    eps_init = eps.quad_count(tree.epsilon, 0, tree.prof_max, tree.ratio_count)

    # Data
    abilities = Ability.objects.all()
    workers = Worker.objects.all()
    workers_len = Worker.objects.all().count()

    # Initial node creation
    node = init_node(workers_len, eps_init)

    # Initial tree creation
    ktree = kdtree(workers=workers,
                   workers_len=workers_len,
                   dim=abilities,
                   prof_act=tree.prof_max,
                   prof_max=tree.prof_max,
                   bins=tree.bins,
                   epsilon=tree.epsilon,
                   ratio_count=tree.ratio_count,
                   node=node,
                   strat_eps_count=eps.quad_count,
                   strat_eps_med=eps.uniform_median,
                   strat_dim=dim.next_unused,
                   calc_med=median.noisy_hist,
                   calc_count=count.noisy,
                   fanout=2,
                   coalition=tree.coalition,
                   meta=tree
                   )

    # End of generation
    tree.root = ktree
    tree.generating = False
    tree.save()
    return


@task(name="task_file_import")
def task_file_import(pk, link):
    """
    Import a CSV file in the workers set
    :param pk:
    :return:
    """
    file = WorkersCSVFile.objects.get(pk=pk)
    file.importing = True
    file.save()

    for k in link:
        link[k] = Ability.objects.get(pk=link[k])
    userid = None
    worker = None

    print("-----")
    print("start import")
    i = 0

    wusers = {}
    wabs = []

    try:
        with open(file.file.path, "r") as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                user = row[0]
                tag = row[1]
                level = row[2]

                if userid != user:
                    i += 1

                    if i % 10000 == 0:
                        Worker.objects.bulk_create(wusers.values())
                        wabs = [
                            WorkerAbility(worker=wusers[u], ability=a, level=l)
                            for u, a, l in wabs
                        ]
                        WorkerAbility.objects.bulk_create(wabs)
                        wusers = {}
                        wabs = []
                        print(i)

                    userid = user
                    worker = Worker(real=False)
                    wusers[i] = worker

                ability = link.get(tag)
                if ability:
                    wabs.append((i, ability, level))

        Worker.objects.bulk_create(wusers.values())
        wabs = [
            WorkerAbility(worker=wusers[u], ability=a, level=l)
            for u, a, l in wabs
        ]
        WorkerAbility.objects.bulk_create(wabs)
    except:
        pass
    file.importing = False
    print("imported")
    print("-----")
