from django.db.models import Avg
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy

from crowdpriv.models import Settings, InitPhase, Ability, Worker, Task, WorkersTreeMeta


class PhaseMixin:
    """
    Base class for initialization mixins
    """
    urls = {
        InitPhase.START: ['initialization'],
        InitPhase.ABILITY: ['ability', 'add-ability', 'del-ability'],
        InitPhase.WORKERS: ['workers', 'workers-add', 'workers-gen',
                            'workers-del', 'workers-del-all'],
        InitPhase.TREE: ['trees'],
        InitPhase.TASKS: ['tasks', 'tasks-add', 'tasks-del'],
        InitPhase.END: ['end']
    }

    @staticmethod
    def get_settings():
        settings, c = Settings.objects.get_or_create(pk=1)
        return settings


class OnPhaseMixin(PhaseMixin):
    """ Give access to the views only if on the defined phase.
        Redirect to the current phase view otherwise.
    """
    allowed_phases = None

    def dispatch(self, request, *args, **kwargs):
        phase = self.get_settings().init

        for allowed in self.allowed_phases:
            if InitPhase.is_same(phase, allowed):
                return super().dispatch(request, *args, **kwargs)
        return HttpResponseRedirect(reverse_lazy(self.urls[phase][0]))


class SummaryMixin:
    """
    Load the information used in the summary
    """

    def get_context_data(self, *args, **kwargs):
        try:
            context = super().get_context_data(*args, **kwargs)
        except AttributeError:
            context = {}

        abilities = Ability.objects.all().annotate(workers_avg=Avg('workers__level'),
                                                   tasks_avg_min=Avg('tasks__level_min'),
                                                   tasks_avg_max=Avg('tasks__level_max')
                                                   )
        workers_done = False
        tasks_done = False
        if len(abilities) > 0:
            workers_done = abilities[0].workers_avg is not None
            tasks_done = abilities[0].tasks_avg_min is not None

        workers_count = Worker.objects.all().count()
        trees_count = WorkersTreeMeta.objects.all().count()
        tasks_count = Task.objects.all().count()

        context['summary'] = {
            'phase': PhaseMixin.get_settings().init,
            'abilities': abilities,
            'workers': {
                'done': workers_done,
                'count': workers_count
            },
            'trees': {
                'count': trees_count
            },
            'tasks': {
                'done': tasks_done,
                'count': tasks_count
            }
        }
        return context

