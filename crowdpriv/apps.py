from django.apps import AppConfig


class CrowdprivConfig(AppConfig):
    name = 'crowdpriv'
