# Crowdpriv

The code itself

## Architecture

- migrations : django stuff (database migrations), do not touch
- static : Static files (CSS / JS)
- templates : HTML templates files
- templatetags : template language extension
- tree : tree generation code
- views : views code
- forms : forms (users input)
- generations.py : generators (workers & tasks) code
- mixins.py : mixin (views extension) code
- models : models (database ORM description) code
- tasks : celery functions
- urls.py : urls to views mapping