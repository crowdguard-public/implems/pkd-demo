from random import randint
from scipy.stats import uniform, truncnorm

from crowdpriv.models import Ability, Worker, WorkerAbility, Task, TaskAbility


def worker_zero ():
    """
    Workers with no capacities
    :return:
    """
    abilities = Ability.objects.all()

    worker = Worker.objects.create(real=False, completed=True)

    for ability in abilities:
        WorkerAbility.objects.create(worker=worker, ability=ability, level=0)


def workers_set_one_spe():
    """
    Workers specialized in one field
    :return:
    """
    abilities = Ability.objects.all()
    dim = len(abilities)

    spe = randint(0, dim - 1)
    levels = uniform.rvs(0, 0.5, dim)
    levels[spe] = uniform.rvs(0.5, 0.5, 1)[0]

    worker = Worker.objects.create(real=False, completed=True)
    for j in range(dim):
        WorkerAbility.objects.create(worker=worker, ability=abilities[j], level=levels[j])


def workers_set_truncnorm_low():
    """
    :return:
    """
    abilities = Ability.objects.all()
    dim = len(abilities)

    levels = truncnorm.rvs(0, 1, 0, 0.25, dim)

    worker = Worker.objects.create(real=False, completed=True)
    for j in range(dim):
        WorkerAbility.objects.create(worker=worker, ability=abilities[j], level=levels[j])


def workers_set_truncnorm_centered():
    abilities = Ability.objects.all()
    dim = len(abilities)

    levels = truncnorm.rvs(0, 1, 0.5, 0.25, dim)

    worker = Worker.objects.create(real=False, completed=True)
    for j in range(dim):
        WorkerAbility.objects.create(worker=worker, ability=abilities[j], level=levels[j])


def workers_set_unif ():
    """
    Random workers
    :return:
    """
    abilities = Ability.objects.all()
    dim = len(abilities)

    levels = uniform.rvs(0, 1, dim)

    worker = Worker.objects.create(real=False, completed=True)
    for j in range(dim):
        WorkerAbility.objects.create(worker=worker, ability=abilities[j], level=levels[j])


def generate_workers(num, method_name):
    """
    Generate num workers following the method
    :param num: number of workers
    :param method_name: generation method name
    :return:
    """
    methods = {
        'zero': worker_zero,
        'one_spe': workers_set_one_spe,
        'unif': workers_set_unif,
        'norm_low': workers_set_truncnorm_low,
        'norm_centered': workers_set_truncnorm_centered
    }

    method_fn = methods.get(method_name)

    if method_fn:
        for i in range(num):
            method_fn()

    return method_fn is not None


def tasks_random(abilities, task):
    """ Generate a new random task
    """
    for ability in abilities:
        levels = sorted(uniform.rvs(0, 1, 2))
        TaskAbility.objects.create(task=task, ability=ability, level_min=levels[0], level_max=levels[1])
    return task


def tasks_update(abilities, task):
    """
    Generate a new random task with ignored abilities
    :param abilities:
    :param task:
    :return:
    """
    for ability in abilities:
        if random.random() > 0.8:
            levels = sorted(uniform.rvs(0, 1, 2))
            TaskAbility.objects.create(task=task, ability=ability, level_min=levels[0], level_max=levels[1])
        else:
            TaskAbility.objects.create(task=task, ability=ability, level_min=0, level_max=1)
    return task


def tasks_one_spe(abilities, task):
    """

    :param abilities:
    :param task:
    :return:
    """
    dim = len(abilities)
    spe = random.randint(0, dim - 1)

    for j in range(dim):
        if j == spe:
            TaskAbility.objects.create(task=task, ability=abilities[j], level_min=uniform.rvs(0.5, 0.5, 1)[0], level_max=1)
        else:
            TaskAbility.objects.create(task=task, ability=abilities[j], level_min=0, level_max=uniform.rvs(0, 0.5, 1)[0])
    return task


def tasks_square_corner(abilities, task):
    """

    :param abilities:
    :param task:
    :return:
    """
    dim = len(abilities)

    budget = uniform.rvs(0.1, 0.4)
    extr = randint.rvs(0, 2, 0, dim)

    for j in range(dim):
        if extr[j] == 0:
            TaskAbility.objects.create(task=task, ability=abilities[j], level_min=0, level_max=budget)
        else:
            TaskAbility.objects.create(task=task, ability=abilities[j], level_min=(1 - budget), level_max=1)
    return task


def tasks_square(abilities, task):
    """

    :param abilities:
    :param task:
    :return:
    """
    dim = len(abilities)
    budget = uniform.rvs(0.1, 0.4)
    begin = uniform.rvs(0, 1 - budget, dim)

    for j in range(dim):
        TaskAbility.objects.create(task=task, ability=abilities[j], level_min=begin[j], level_max=(begin[j] + budget))
    return task


def tasks_square_3cases(abilities, task):
    dim = len(abilities)
    budget = uniform.rvs(0.1, 0.4)
    extr = randint.rvs(0, 3, 0, dim)

    for j in range(dim):
        if extr[j] == 0:
            TaskAbility.objects.create(task=task, ability=abilities[j], level_min=0, level_max=budget)
        elif extr[j] == 1:
            TaskAbility.objects.create(task=task, ability=abilities[j], level_min=(1 - budget), level_max=1)
        else:
            begin = uniform.rvs(0, 1 - budget, 1)[0]
            TaskAbility.objects.create(task=task, ability=abilities[j], level_min=begin, level_max=(begin + budget))
    return task


def generate_tasks(num, method_name):
    """
    Generate num tasks following the method
    :param num: number of tasks
    :param method_name: generation method name
    :return:
    """
    methods = {
        'random': tasks_random,
        'pareto': tasks_update,
        'one_spe': tasks_one_spe,
        'random_square_corner': tasks_square_corner,
        'random_square': tasks_square,
        'random_square_3cases': tasks_square_3cases
    }

    method_fn = methods.get(method_name)

    if method_fn:
        abilities = Ability.objects.all()
        dim = len(abilities)

        for i in range(num):
            task = Task.objects.create(title="", descr="")
            task = method_fn(abilities, task)
            task.title = "Task " + str(task.id)
            task.descr = "Generated task"
            task.save()

    return method_fn is not None

