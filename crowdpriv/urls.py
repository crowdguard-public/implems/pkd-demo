from django.urls import path

from crowdpriv.views.misc import *
from crowdpriv.views.abilities import *
from crowdpriv.views.workers import *
from crowdpriv.views.trees import *
from crowdpriv.views.tasks import *

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    # Initialization views (misc)
    path('init', InitializationView.as_view(), name='initialization'),
    path('init/end', InitEndView.as_view(), name='end'),
    path('init/reset', InitResetView.as_view(), name='reset'),

    # Abilities views
    path('init/ability', AbilitiesListView.as_view(), name="ability"),
    path('init/ability/add', AbilityAddView.as_view(), name="add-ability"),
    path('init/ability/del/<int:pk>', AbilityDelView.as_view(), name="del-ability"),

    # Workers views
    path('init/workers', WorkersListView.as_view(), name='workers'),
    path('init/workers/add', WorkerAddView.as_view(), name='workers-add'),
    path('init/workers/gen', WorkersGenerateView.as_view(), name='workers-gen'),
    path('init/workers/import/upload', WorkersImportUploadView.as_view(), name='workers-upload'),
    path('init/workers/import/<int:pk>', WorkersImportView.as_view(), name="workers-import"),
    path('init/workers/del/<int:pk>', WorkerDelView.as_view(), name='workers-del'),
    path('init/workers/del/all', WorkersDelAllView.as_view(), name='workers-del-all'),

    # Trees views
    path('init/trees', TreesListView.as_view(), name='trees'),
    path('init/trees/add', TreeAddView.as_view(), name='trees-add'),
    path('init/trees/del/<int:pk>', TreeDelView.as_view(), name='trees-del'),
    path('init/trees/del/all', TreesDelAllView.as_view(), name='trees-del-all'),
    path('init/trees/<int:pk>', TreeView.as_view(), name='trees-get'),
    path('init/trees/<int:pk>/gen', TreeGenerationInfoView.as_view(), name='trees-get-info'),
    path('init/trees/node/<int:pk>', TreeNodeWorkersView.as_view(), name='trees-node'),

    # Task views
    path('init/tasks', TasksListView.as_view(), name='tasks'),
    path('init/tasks/helperapi', TaskHelperAPIView.as_view(), name='tasks-helper-api'),
    path('init/tasks/add', TaskAddView.as_view(), name='tasks-add'),
    path('init/tasks/gen', TasksGenerateView.as_view(), name='tasks-gen'),
    path('init/tasks/del/<int:pk>', TaskDelView.as_view(), name='tasks-del'),
    path('init/tasks/del/all', TasksDelAllView.as_view(), name='tasks-del-all')
]
