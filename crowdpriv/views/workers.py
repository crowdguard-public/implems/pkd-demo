from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, DeleteView
import csv

from crowdpriv.forms import WorkerAbilitiesForm, WorkersGenerationForm, WorkersFileForm, WorkersImportForm
from crowdpriv.generation import generate_workers
from crowdpriv.mixins import OnPhaseMixin, SummaryMixin
from crowdpriv.models import InitPhase, Worker, Ability, WorkerAbility, WorkersCSVFile
from crowdpriv.tasks import task_file_import


class WorkersListView(OnPhaseMixin, SummaryMixin, ListView):
    """
    List the available workers.
    """
    allowed_phases = (InitPhase.WORKERS, InitPhase.TREE, InitPhase.TASKS)
    model = Worker
    template_name = 'workers/workers.html'
    paginate_by = 10

    def post(self, request):
        settings = self.get_settings()
        if settings.init == InitPhase.WORKERS:
            settings.init = InitPhase.TREE
            settings.save()
        return HttpResponseRedirect(reverse_lazy('trees'))


class WorkerAddView(OnPhaseMixin, View):
    """
    Create a new fake worker
    """
    allowed_phases = (InitPhase.WORKERS, )

    def get(self, request):
        return render(request, template_name='workers/worker_add.html', context={
            'abilities': Ability.objects.all()
        })

    def post(self, request):
        form = WorkerAbilitiesForm(data=request.POST)

        if form.is_valid():
            worker = Worker.objects.create(real=False)

            abilities = Ability.objects.all()
            for ability in abilities:
                fname = 'ab_' + str(ability.id)
                level = form.cleaned_data.get(fname)

                if level == None:
                    worker.abilities.all().delete()
                    worker.delete()
                    break

                WorkerAbility.objects.create(worker=worker, ability=ability, level=level)

            worker.completed = True
            worker.save()

            return HttpResponseRedirect(reverse_lazy('workers'))

        return self.get(request)


class WorkersGenerateView(OnPhaseMixin, View):
    """
    Generate numerous fake workers
    """
    allowed_phases = (InitPhase.WORKERS, )

    def get(self, request):
        return render(request, template_name='workers/worker_generate.html', context={
            'form': WorkersGenerationForm()
        })

    def post(self, request):
        form = WorkersGenerationForm(request.POST)
        if form.is_valid():
            num = form.cleaned_data['num']
            method = form.cleaned_data['method']

            if generate_workers(num, method):
                return HttpResponseRedirect(reverse_lazy('workers'))

        return render(request, template_name='workers/worker_generate.html', context={
            'form': form
        })


class WorkersImportUploadView(OnPhaseMixin, View):
    """ Upload a workers CSV file """
    allowed_phases = (InitPhase.WORKERS, )

    def get(self, request):
        return render(request, template_name='workers/import_upload.html', context={
            "form": WorkersFileForm()
        })

    def post(self, request):
        form = WorkersFileForm(request.POST, request.FILES)

        if form.is_valid():
            o = form.save()
            return redirect('workers-import', pk=o.id)
        else:
            return render(request, template_name='workers/import_upload.html', context={
                "form": form
            })


class WorkersImportView(OnPhaseMixin, View):
    """ Import a workers CSV file """
    allowed_phases = (InitPhase.WORKERS, )

    def get_tags(self, file: WorkersCSVFile):
        userid = None
        tags = []
        with open(file.file.path, "r") as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                user = row[0]
                tag = row[1]

                if not userid:
                    userid = user
                if userid != user:
                    break
                tags += [tag]
        return tags

    def get(self, request, pk=None):
        file = get_object_or_404(WorkersCSVFile, id=pk)
        tags = self.get_tags(file)
        skills = Ability.objects.all()

        return render(request, template_name='workers/import.html', context={
            'tags': tags,
            'skills': skills,
            'form': WorkersImportForm(abilities=skills, tags=tags)
        })

    def post(self, request, pk=None):
        file = get_object_or_404(WorkersCSVFile, id=pk)
        tags = self.get_tags(file)
        skills = Ability.objects.all()
        form = WorkersImportForm(data=request.POST, abilities=skills, tags=tags)

        if form.is_valid():
            link = form.make_link()
            task_file_import.delay(pk, link)
            return redirect('workers')
        else:
            return render(request, template_name='workers/import.html', context={
                'tags': tags,
                'skills': skills,
                'form': form
            })


class WorkerDelView(OnPhaseMixin, DeleteView):
    """
    Remove a fake worker
    """
    allowed_phases = (InitPhase.WORKERS, )
    model = Worker
    template_name = 'workers/worker_del.html'
    success_url = reverse_lazy('workers')


class WorkersDelAllView(OnPhaseMixin, View):
    """
    Remove all workers
    """
    allowed_phases = (InitPhase.WORKERS, )

    def get(self, request):
        return render(request, template_name='workers/worker_del_all.html')

    def post(self, request):
        Worker.objects.all().delete()
        return HttpResponseRedirect(reverse_lazy('workers'))

