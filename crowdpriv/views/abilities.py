from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import ListView, FormView, DeleteView

from crowdpriv.forms import AddAbilityForm
from crowdpriv.mixins import OnPhaseMixin, SummaryMixin
from crowdpriv.models import InitPhase, Ability


class AbilitiesListView(OnPhaseMixin, SummaryMixin, ListView):
    """
    List the available utilities and propose options to manage them
    """
    allowed_phases = (InitPhase.ABILITY, InitPhase.WORKERS, InitPhase.TREE, InitPhase.TASKS)
    model = Ability
    template_name = "ability/ability.html"
    paginate_by = 10

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['nok'] = Ability.objects.count() == 0

        return context

    def post(self, request, *args, **kwargs):
        if Ability.objects.count() == 0:
            return self.get(request, *args, **kwargs)

        settings = self.get_settings()

        if settings.init == InitPhase.ABILITY:
            settings.init = InitPhase.WORKERS
            settings.save()
        return HttpResponseRedirect(reverse_lazy('workers'))


class AbilityAddView(OnPhaseMixin, FormView):
    """
    Add a new ability
    """
    allowed_phases = (InitPhase.ABILITY, )
    template_name = 'ability/ability_add.html'
    form_class = AddAbilityForm
    success_url = reverse_lazy('ability')

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class AbilityDelView(OnPhaseMixin, DeleteView):
    """
    Remove an ability
    """
    allowed_phases = (InitPhase.ABILITY, )
    model = Ability
    template_name = 'ability/ability_del.html'
    success_url = reverse_lazy('ability')

