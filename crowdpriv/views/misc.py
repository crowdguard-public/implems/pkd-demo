from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import TemplateView

from crowdpriv.mixins import OnPhaseMixin
from crowdpriv.models import InitPhase, Ability, Worker, Task, WorkersTreeMeta, WorkersTreeNode, Settings


class IndexView(OnPhaseMixin, TemplateView):
    template_name = "index.html"
    allowed_phases = (InitPhase.OVER, )


class InitializationView(OnPhaseMixin, TemplateView):
    template_name = 'init/initialization.html'
    allowed_phases = (InitPhase.START, InitPhase.ABILITY, InitPhase.WORKERS, InitPhase.TREE, InitPhase.TASKS )

    def post(self, request):
        settings = self.get_settings()
        if settings.init == InitPhase.START:
            settings.init = InitPhase.ABILITY
            settings.save()
        return HttpResponseRedirect(reverse_lazy('ability'))


class InitEndView(OnPhaseMixin, View):
    """
    Last initialization view.
    Terminate the initialization
    """
    allowed_phases = (InitPhase.END, )

    def get(self, request):
        return render(request, template_name="init/end.html")

    def post(self, request):
        return HttpResponseRedirect(reverse_lazy('reset'))


class InitResetView(View):
    """
    Reset the app configuration
    """

    def get(self, request):
        return render(request, template_name='init/reset.html')

    def post(self, request):
        Worker.objects.all().delete()
        Ability.objects.all().delete()
        Task.objects.all().delete()
        WorkersTreeMeta.objects.all().delete()
        WorkersTreeNode.objects.all().delete()
        Settings.objects.all().delete()
        return HttpResponseRedirect(reverse_lazy('initialization'))
