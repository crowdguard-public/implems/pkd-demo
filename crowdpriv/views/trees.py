from django.db import IntegrityError
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, DeleteView

from crowdpriv.forms import TreeForm
from crowdpriv.mixins import OnPhaseMixin, SummaryMixin
from crowdpriv.models import WorkersTreeMeta, InitPhase, WorkersTreeNode, Ability
from crowdpriv.tasks import task_tree_generation


class TreesListView(OnPhaseMixin, SummaryMixin, ListView):
    """
    Views the current trees
    """
    allowed_phases = (InitPhase.TREE, InitPhase.TASKS)
    model = WorkersTreeMeta
    template_name = 'trees/trees.html'
    paginate_by = 10

    def post(self, request):
        settings = self.get_settings()
        if settings.init == InitPhase.TREE:
            settings.init = InitPhase.TASKS
            settings.save()
        return HttpResponseRedirect(reverse_lazy(self.urls[settings.init][0]))


class TreeAddView(OnPhaseMixin, View):
    allowed_phases = (InitPhase.TREE, )

    def get(self, request):
        return render(request, template_name='trees/tree_add.html', context={
            'form': TreeForm()
        })

    def post(self, request):
        form = TreeForm(request.POST)

        if form.is_valid():
            epsilon = form.cleaned_data['epsilon']
            prof_max = form.cleaned_data['prof_max']
            bins = form.cleaned_data['bins']
            ratio_count = form.cleaned_data['ratio']
            coalition = form.cleaned_data['coalition']

            # Expected number of nodes
            expected = 0
            for i in range(prof_max + 1):
                expected += 2 ** i

            try:
                tree = WorkersTreeMeta.objects.create(epsilon=epsilon, prof_max=prof_max,
                                                      bins=bins, ratio_count=ratio_count,
                                                      coalition=coalition, expected=expected)
                task_tree_generation.delay(tree.pk)
                return HttpResponseRedirect(reverse_lazy('trees'))
            except IntegrityError as e:
                print(e)
                form.add_error(None, "There is already a tree for this set of parameters.")
            except Exception as e:
                print(e)
                form.add_error(None, "Unhandled error, check logs for more details.")

        return render(request, template_name='trees/tree_add.html', context={
            'form': form
        })


class TreeGenerationInfoView(OnPhaseMixin, View):
    """
    API call to get information about a generation tree
    """
    allowed_phases = (InitPhase.TREE, )

    def get(self, request, pk=None):
        tree = get_object_or_404(WorkersTreeMeta, pk=pk)

        return JsonResponse({
            'generating': tree.generating,
            'current': tree.current,
            'expected': tree.expected
        })


class TreeDelView(OnPhaseMixin, DeleteView):
    """
    Delete a generated tree
    """
    allowed_phases = (InitPhase.TREE, )
    model = WorkersTreeMeta
    template_name = 'trees/tree_del.html'
    success_url = reverse_lazy('trees')

    def get_queryset(self):
        qs = super().get_queryset()
        # TODO uncomment the filter (commented for debug purpose only)
        # qs = qs.filter(generating=False)
        return qs


class TreeView(OnPhaseMixin, SummaryMixin, View):
    """
    View a specific tree
    """
    allowed_phases = (InitPhase.TREE, InitPhase.TASKS)

    def get(self, request, pk=None):
        tree = get_object_or_404(WorkersTreeMeta, pk=pk)
        context = self.get_context_data()
        context['tree'] = tree

        return render(request, template_name='trees/tree_view.html', context=context)


class TreeNodeWorkersView(OnPhaseMixin, SummaryMixin, ListView):
    """
    View the workers in a specific tree node
    """
    allowed_phases = (InitPhase.TREE, InitPhase.TASKS)
    template_name = "trees/tree_node_workers.html"
    paginate_by = 10

    def get_queryset(self):
        pk = self.kwargs['pk']
        node = get_object_or_404(WorkersTreeNode, pk=pk)
        return node.peoples.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs['pk']
        node = get_object_or_404(WorkersTreeNode, pk=pk)
        context['node'] = node
        return context


class TreesDelAllView(OnPhaseMixin, View):
    """ Remove all generated trees.
        Keep the trees who are currently being generated
    """
    allowed_phases = (InitPhase.TREE, )

    def get(self, request):
        return render(request, template_name='trees/tree_del_all.html')

    def post(self, request):
        # TODO a better suppression algorithm since WorkersTreeNode are kept
        WorkersTreeMeta.objects.exclude(generating=True).delete()
        return HttpResponseRedirect(reverse_lazy('trees'))
