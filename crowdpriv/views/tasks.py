import json

from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, DeleteView
from django.db.models import Q

from crowdpriv.forms import TaskForm, WorkersGenerationForm, TasksGenerationForm, TaskHelperAPIForm, TaskHelperAPI_2Form
from crowdpriv.generation import generate_tasks
from crowdpriv.mixins import OnPhaseMixin, SummaryMixin
from crowdpriv.models import InitPhase, Task, Ability, TaskAbility, WorkersTreeMeta, WorkersTree, Worker


class TasksListView(OnPhaseMixin, SummaryMixin, ListView):
    """
    List the current tasks
    """
    allowed_phases = (InitPhase.TASKS, )
    model = Task
    template_name = 'tasks/tasks.html'
    paginate_by = 10

    def get_queryset(self):
        queryset = super().get_queryset()

        try:  # Filter by tree
            tree = int(self.request.GET.get('tree'))
            tree = WorkersTreeMeta.objects.get(pk=tree)
            queryset = queryset.filter(tree=tree)
        except:
            pass
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['trees'] = WorkersTreeMeta.objects.all()
        return context

    def post(self, request):
        settings = self.get_settings()
        if settings.init == InitPhase.TASKS:
            settings.init = InitPhase.END
            settings.save()
        return HttpResponseRedirect(reverse_lazy(self.urls[settings.init][0]))


class TaskHelperAPIView(OnPhaseMixin, View):
    """
    Task helper. Get the nodes inside an area defined by the audiences
    """
    allowed_phases = (InitPhase.TASKS,)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request):
        abilities = Ability.objects.all()
        trees = WorkersTreeMeta.objects.all()
        data = json.loads(request.body)
        form = TaskHelperAPI_2Form(data=data, abilities=abilities, trees=trees)

        if form.is_valid():
            # Load the task area
            area = {
                a: [
                    form.cleaned_data['ab_min_' + str(a.id)],
                    form.cleaned_data['ab_max_' + str(a.id)]
                ]
                for a in abilities
            }

            tree = get_object_or_404(WorkersTreeMeta, id=form.cleaned_data['tree'])

            real, pert, nodes = TaskHelperAPIView.get_nodes(tree.root, area)


            return JsonResponse({
                'error': None,
                'tree_pert_count': pert,
                'tree_real_count':  real,
                'nodes': nodes,
                'real_count': TaskHelperAPIView.get_real_count(area)
            })

        return JsonResponse({
            'error': form.errors
        })


    @staticmethod
    def get_nodes(tree: WorkersTree, area):
        """
        Get the leaf nodes on a WorkerTree who may contain workers from the defined area
        :param tree: node to search
        :param area: exepected task area (dict Ability -> [min, max])
        :return: (unperturbed number of workers, pernurbed number of workers, number of nodes)
        """
        # Check if the node is a leaf
        if tree.children.all().count() == 0:
            # Check if the area defined by the leaf is partly in the area requested by the user
            for ability in area:
                volume = tree.node.volume.get(ability=ability)
                min = area[ability][0]
                max = area[ability][1]

                if not ( (min <= volume.min < max) or (min <= volume.max < max) or
                         (min == volume.max == 1) or (max == volume.max == 1)
                        ):
                    return 0, 0, 0

            # The leaf is partly inside the task area -> get the proportion of the task inside and approximate the number of workers concerned
            it = 0  # Area inside taks
            iv = 0  # Area inside node

            for ability in area:
                volume = tree.node.volume.get(ability=ability)
                min = area[ability][0]
                max = area[ability][1]

                imin = min
                if volume.min > min:
                    imin = volume.min

                imax = max
                if volume.max < max:
                    imax = volume.max

                if it == 0:
                    it = imax - imin
                else:
                    it *= imax - imin

                if iv == 0:
                    iv = volume.max - volume.min
                else:
                    iv *= volume.max - volume.min

            if iv > 0:
                pi = it / iv
            else:
                pi = 0

            return tree.node.real_count() * pi, tree.node.app_count * pi, 1

        else:
            real = 0
            pert = 0
            nodes = 0
            # Get the potentials matching nodes in the childrens of this node
            for child in tree.children.all():
                c_real, c_pert, c_nodes = TaskHelperAPIView.get_nodes(child, area)
                real += c_real
                pert += c_pert
                nodes += c_nodes

            return real, pert, nodes


    @staticmethod
    def get_real_count(area):
        qs = Worker.objects.all()

        for ability in area:
            min = area[ability][0]
            max = area[ability][1]

            qs = qs.filter(
                Q(abilities__ability=ability) &
                Q(
                    Q(
                        Q(abilities__level__gte=min) &
                        Q(abilities__level__lt=max)
                    ) |
                    Q(
                        Q(abilities__level=max) &
                        Q(abilities__level=1)
                    )
                )
            )
        return qs.count()


class TaskAddView(OnPhaseMixin, View):
    """
    Add a new task
    """
    allowed_phases = (InitPhase.TASKS, )

    def get(self, request):
        abilities = Ability.objects.all()
        trees = WorkersTreeMeta.objects.all()
        return render(request, template_name='tasks/task_add.html', context={
            'form': TaskForm(abilities=abilities, trees=trees),
            'abilities': abilities,
            'trees': trees
        })

    def post(self, request):
        abilities = Ability.objects.all()
        trees = WorkersTreeMeta.objects.all()
        form = TaskForm(data=request.POST, abilities=abilities, trees=trees)

        if form.is_valid():
            title = form.cleaned_data['title']
            descr = form.cleaned_data['descr']
            tree = form.cleaned_data['tree']
            tree = get_object_or_404(WorkersTreeMeta, pk=tree)

            task = Task.objects.create(title=title, descr=descr, tree=tree)
            area = {}

            for ability in abilities:
                fn_min = 'min_' + str(ability.id)
                fn_max = 'max_' + str(ability.id)

                f_min = form.cleaned_data.get(fn_min)
                f_max = form.cleaned_data.get(fn_max)
                area[ability] = [f_min, f_max]

                if f_min is None or f_max is None:
                    task.abilities.all().delete()
                    task.delete()
                    return render(request, template_name='tasks/task_add.html', context={
                        'form': TaskForm(),
                        'abilities': Ability.objects.all()
                    })

                if f_min > f_max:
                    f_min, f_max = f_max, f_min

                TaskAbility.objects.create(task=task, ability=ability, level_min=f_min, level_max=f_max)

            unpert, pert, nodes = TaskHelperAPIView.get_nodes(tree.root, area)
            real = TaskHelperAPIView.get_real_count(area)

            task.real_count = real
            task.nodes = nodes
            task.unpert_count = unpert
            task.pert_count = pert
            task.save()

            return HttpResponseRedirect(reverse_lazy('tasks'))

        print(form.errors)
        return render(request, template_name='tasks/task_add.html', context={
            'form': form,
            'abilities': abilities,
            'trees': trees
        })


class TasksGenerateView(OnPhaseMixin, View):
    """
    Generate numerous fake workers
    """
    allowed_phases = (InitPhase.TASKS, )

    def get(self, request):
        return render(request, template_name='tasks/tasks_gen.html', context={
            'form': WorkersGenerationForm()
        })


    def post(self, request):
        form = TasksGenerationForm(request.POST)
        if form.is_valid():
            num = form.cleaned_data['num']
            method = form.cleaned_data['method']

            if generate_tasks(num, method):
                return HttpResponseRedirect(reverse_lazy('tasks'))

        return render(request, template_name='tasks/tasks_gen.html', context={
            'form': form
        })


class TaskDelView(OnPhaseMixin, DeleteView):
    """
    Remove a task
    """
    allowed_phases = (InitPhase.TASKS, )
    model = Task
    template_name = 'tasks/task_del.html'
    success_url = reverse_lazy('tasks')


class TasksDelAllView(OnPhaseMixin, View):
    """
    Remove all workers
    """
    allowed_phases = (InitPhase.TASKS, )

    def get(self, request):
        return render(request, template_name='tasks/task_del_all.html')

    def post(self, request):
        Task.objects.all().delete()
        return HttpResponseRedirect(reverse_lazy('tasks'))
