from django.db.models import F

from crowdpriv.models import WorkersTreeNode, Ability, Worker, WorkersTree, WorkerTreeNodeVolume, WorkersTreeMeta
from crowdpriv.tree import divide_users


def init_node(workers_len, eps_init: float) -> WorkersTreeNode:
    """
    Task tree generation.
    Create the initial node
    :param workers_len: Number of workers
    :param eps_init: EPS value
    :return: the new node
    """
    alpha = (eps_init ** 2) * workers_len

    node = WorkersTreeNode.objects.create(app_count=workers_len,
                                          epsilon=eps_init,
                                          alpha=alpha
                                          )
    node.peoples.set(Worker.objects.all())

    # Initialize the volume
    for ability in Ability.objects.all():
        vol, c = WorkerTreeNodeVolume.objects.get_or_create(ability=ability, min=0, max=1)
        node.volume.add(vol)

    return node


def kdtree(workers: [Worker], workers_len: int, dim: [Ability], prof_act: int, prof_max: int, bins, epsilon: float,
           ratio_count: float, node: WorkersTreeNode, strat_eps_count, strat_eps_med, strat_dim, calc_med, calc_count,
           fanout: int, coalition: int, meta:WorkersTreeMeta):
    """
    kdtree calculation
    :param workers: List of available workers
    :param workers_len: Number of workers in the workers set
    :param dim: List of available abilities
    :param prof_act: Current height
    :param prof_max: Maximum height
    :param bins:
    :param epsilon:
    :param ratio_count:
    :param node:
    :param strat_eps_count: Function
    :param strat_eps_med: Function
    :param strat_dim: Function
    :param calc_med: Function
    :param calc_count: Function
    :param fanout: Number of children in the tree
    :param coalition: Number of attackers
    :param meta: Tree metadata
    :return:
    """
    print("------")
    print("Node : " + str(prof_act))
    meta.current = F('current') + 1
    meta.save(update_fields=['current'])

    if prof_act == 0:
        node.z = node.alpha
        node.save()

        tree = WorkersTree.objects.create(node=node,
                                          cut=None)
        return tree
    else:
        users_inside = node.peoples.all()

        # Dimension to divide
        v = node.volume.all()
        v_len = v.count()
        new_dim: int = strat_dim(v_len, prof_act, prof_max)
        new_dim: Ability = dim[new_dim]

        # MEDIAN
        e_med: float = strat_eps_med(epsilon, prof_max, ratio_count)
        med = calc_med(e_med, new_dim, dim, v, workers, bins, fanout, coalition)

        new_vs: [[WorkerTreeNodeVolume]] = []
        eps_count = fanout * [e_med]

        alpha = node.alpha
        tree_table = []

        for i in range(fanout):
            new_vs_fn_vols = {}
            for ability in dim:
                if ability.id == new_dim.id:
                    # Create the new volume for the new dim
                    volume, c = WorkerTreeNodeVolume.objects.get_or_create(ability=new_dim,
                                                                           min=med[0][i],
                                                                           max=med[0][i + 1])
                else:
                    volume = v.get(ability=ability)
                new_vs_fn_vols[ability] = volume
            new_vs += [new_vs_fn_vols]

        pop_inside = divide_users.divide(users_inside, new_vs, dim)

        for i in range(fanout):
            if ratio_count != 0:
                eps_count[i] = strat_eps_count(epsilon, prof_act, prof_max, ratio_count)
                count_aprox = calc_count(eps_count[i], new_vs[i], workers, dim, coalition)

            # Alpha (consistency)
            alpha_children = alpha + (eps_count[i] ** 2 * count_aprox)

            # Create child
            child = WorkersTreeNode.objects.create(app_count=count_aprox,
                                                   epsilon=eps_count[i],
                                                   alpha=alpha_children)

            new_volume: [WorkerTreeNodeVolume] = [new_vs[i][k] for k in new_vs[i]]
            child.volume.add(* new_volume)

            child.peoples.add(* pop_inside[i])

            tree_table += [kdtree(workers=workers,
                                  workers_len=workers_len,
                                  dim=dim,
                                  prof_act=(prof_act - 1),
                                  prof_max=prof_max,
                                  bins=bins,
                                  epsilon=epsilon,
                                  ratio_count=ratio_count,
                                  node=child,
                                  strat_eps_count=strat_eps_count,
                                  strat_eps_med=strat_eps_med,
                                  strat_dim=strat_dim,
                                  calc_med=calc_med,
                                  calc_count=calc_count,
                                  fanout=fanout,
                                  coalition=coalition,
                                  meta=meta
                                  )]

        tree = WorkersTree.objects.create(node=node,
                                          cut=new_dim,
                                          table_cut=str(med[0][1:-1])
                                          )
        tree.children.add(* tree_table)

        return tree

