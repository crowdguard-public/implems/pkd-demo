def constante(eps, h):
    return eps*(2**(1/3)-1)/(2**((h+1)/3)-1)


def quad_count(epsilon, prof_act, prof_max, ratio_count):
    e = epsilon*ratio_count
    c = constante(e, prof_max-1)
    res = 2**((prof_max - prof_act)/3)*c
    return res


def uniform_median(epsilon: float, prof_max: int, ratio_count: float):
    e = epsilon * (1 - ratio_count)
    return e / prof_max
