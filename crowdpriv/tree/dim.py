import random

def zero(volume_len, prof_act, prof_max):
    return 0


def random_dim(volume_len, prof_act, prof_max):
    return random.randint(0, volume_len - 1)


def next_unused(volume_len, prof_act, prof_max):
    return (prof_max - prof_act) % volume_len

