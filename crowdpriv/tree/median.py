import numpy
from scipy.stats import nbinom
from operator import itemgetter

from crowdpriv.models import WorkerTreeNodeVolume, Worker, Ability


def is_in(user: Worker, volume: [WorkerTreeNodeVolume], abilities: [Ability]) -> bool:
    """
    Test if the user is in the volume
    :param user: user to test
    :param volume: volume to test
    :param abilities: volume abilities
    :return:
    """
    return all(volume.get(ability=ability).min <=
               user.abilities.get(ability=ability).level <
               volume.get(ability=ability).max
               for ability in abilities
               )


def precise_med(epsilon, div_dim: Ability, dim, volume, matrix_user, bins, fanout, coalition):
    """
    :param epsilon:
    :param div_dim: Volume ability
    :param dim: Abilities set
    :param volume: Volume set
    :param matrix_user: Workers set
    :param bins:
    :param fanout:
    :param coalition:
    :return:
    """
    # TODO optimize -> replace is_in with request on model
    matrix_user = [user for user in matrix_user if is_in(user, volume, dim)]
    sorted_dim = sorted(matrix_user, key=lambda x: x.abilities.get(ability=div_dim))

    tmp1 = volume.get(ability=div_dim).min  # Correspond to : volume[div_dim][0]
    divisions = [tmp1]
    people_in = len(sorted_dim)

    if people_in == 0:
        return [[tmp1] * (fanout + 1), 0]

    for i in range(fanout -1):
        j = int((i + 1) * people_in / fanout)
        divisions = divisions + [sorted_dim[j].abilities.get(ability=div_dim).level]

    divisions = divisions + [volume.get(ability=div_dim).max]

    return [divisions, people_in / fanout]


def hist(epsilon, div_dim: Ability, dim, volume, matrix_user, bins, fanout, coalition):
    histogram = []
    debut = volume.get(ability=div_dim).min
    fin = volume.get(ability=div_dim).max
    taille_part = (debut - fin) / bins
    part = [debut + i * taille_part for i in range(bins + 1)]

    # Sub-areas counts
    for b in range(bins):
        e = [sum(is_in(user, volume, dim) & (part[b] <= user.abilities.get(ability=div_dim).level < part[b + 1]))
             for user in matrix_user]
        histogram += e

    somme = sum(histogram)
    count_before = 0
    bins_before = 0
    median = []

    for i in range(fanout - 1):
        while bins_before < bins :
            tmp = count_before + histogram[bins_before]
            if tmp >= (i + 1) * somme / fanout:
                break
            bins_before = bins_before + 1
            count_before = tmp

        count_middle = histogram[bins_before]
        if count_middle == 0:
            median = median + [debut + taille_part * (bins_before + ((i + 1) / fanout))]
        else:
            median = median + [debut + taille_part * (bins_before + (1 / fanout * count_middle)) * (somme * (i + 1) - fanout * count_before)]
    return [[debut] + median + [fin], somme / fanout]


def noisy_hist(epsilon: float, div_dim: Ability, dim: [Ability], volume: [WorkerTreeNodeVolume],
               matrix_user: [Worker], bins: int, fanout: int, coalition: int)\
        -> [[float], float]:
    """
        :param epsilon:
        :param div_dim: Volume ability
        :param dim: Abilities set
        :param volume: Volume set (as a queryable object)
        :param matrix_user: Workers set
        :param bins:
        :param fanout:
        :param coalition:
        :return:
    """
    n_user = len(matrix_user)
    histogram = []
    debut = volume.get(ability=div_dim).min
    fin = volume.get(ability=div_dim).max
    taille_part = (fin - debut) / bins

    part = [debut + i * taille_part for i in range(bins + 1)]

    for b in range(bins):
        e = [sum(
            (is_in(user, volume, dim) & (part[b] <= user.abilities.get(ability=div_dim).level < part[b + 1]))
            for user in matrix_user)
        ]
        histogram += e

    # Add noise
    p = 1 - numpy.exp(-epsilon)
    for b in range(bins):
        # Noise generation
        list_noise_plus = nbinom.rvs(1 / (n_user - coalition), p, 0, n_user)
        geom_plus = sum(list_noise_plus)
        list_noise_moins = nbinom.rvs(1 / (n_user - coalition), p, 0, n_user)
        geom_moins = sum(list_noise_moins)
        noise = geom_plus - geom_moins

        histogram[b] = max(histogram[b] + noise, 0)

    somme = sum(histogram)
    count_before = 0
    bins_before = 0
    median = []

    for i in range(fanout - 1):
        while bins_before < bins :
            tmp = count_before + histogram[bins_before]
            if tmp >= (i + 1) * somme / fanout:
                break
            bins_before = bins_before + 1
            count_before = tmp
        count_middle = histogram[bins_before]
        if count_middle == 0:
            median += [debut + taille_part * (bins_before + ((i + 1) / fanout))]
        else:
            median += [debut + taille_part * (bins_before + (1 / (fanout * count_middle)) * (somme * (i + 1) - fanout * count_before))]
    return [[debut] + median + [fin], somme / fanout]

