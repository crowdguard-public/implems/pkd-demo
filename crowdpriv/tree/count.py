import numpy
from scipy.stats import nbinom
from typing import Dict

from crowdpriv.models import Worker, WorkerTreeNodeVolume, Ability


def is_in(user: Worker, volume: Dict[Ability, WorkerTreeNodeVolume], abilities: [Ability]) -> bool:
    """
    Test if the user is in the volume
    :param user: A user
    :param volume: A volume
    :param abilities: Set of abilities
    :return:
    """
    return all(volume.get(ability).min <=
               user.abilities.get(ability=ability).level <
               volume.get(ability).max
               for ability in abilities
               )


def precise(epsilon: float, volume: Dict[Ability, WorkerTreeNodeVolume],
            workers: [Worker], abilites: [Ability], coalition: int) -> int:
    """
    :param epsilon:
    :param volume:
    :param workers:
    :param abilites:
    :param coalition:
    :return:
    """
    return sum(is_in(worker, volume, abilites) for worker in workers)


def noisy(epsilon: float, volume: Dict[Ability, WorkerTreeNodeVolume],
          workers: [Worker], abilites: [Ability], coalition: int) -> int:
    """

    :param epsilon:
    :param volume:
    :param workers:
    :param abilites:
    :param coalition:
    :return:
    """
    workers_len = len(workers)
    p = 1 - numpy.exp(-epsilon)
    list_noise_plus = nbinom.rvs(1 / (workers_len - coalition), p, 0, workers_len)
    geom_plus = sum(list_noise_plus)
    list_noise_moins = nbinom.rvs(1 / (workers_len - coalition), p, 0, workers_len)
    geom_moins = sum(list_noise_moins)
    noise = geom_plus - geom_moins
    return precise(epsilon, volume, workers, abilites, coalition) + noise
