from typing import Dict

from crowdpriv.models import Worker, WorkerTreeNodeVolume, Ability


def is_in(user: Worker, volume: Dict[Ability, WorkerTreeNodeVolume], abilities: [Ability]) -> bool:
    """
    Test if the user is in the volume
    :param user: A user
    :param volume: A volume
    :param abilities: Set of abilities
    :return:
    """
    return all(
        (volume.get(ability).min <= user.abilities.get(ability=ability).level < volume.get(ability).max) or
        (volume.get(ability).max == 1 and
            (volume.get(ability).min <= user.abilities.get(ability=ability).level <= volume.get(ability).max)
        )
        for ability in abilities
        )


def divide(matrix_users: [Worker], volumes: [Dict[Ability, WorkerTreeNodeVolume]], dim: [Ability]) \
        -> [[Worker]]:
    """
    Divide the user set and put them in their volumes
    :param matrix_users: Set of workers
    :param volumes: Set of volumes (list of WorkerTreeNodeVolume)
    :param fanout: len(volumes)
    :param dim: Set of abilities
    :return: List of workers for each volumes
    """
    result = [[] for x in range(len(volumes))]

    for u in matrix_users:
        for i, v in enumerate(volumes):
            if is_in(u, v, dim):
                result[i].append(u)
                break

    return result


