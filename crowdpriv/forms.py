from django import forms
from django.utils.inspect import func_accepts_kwargs

from crowdpriv.models import Ability, WorkersCSVFile


class AddAbilityForm(forms.ModelForm):
    """
    For used to add an ability
    """
    class Meta:
        model = Ability
        fields = ('name', 'descr')


class WorkerAbilitiesForm(forms.Form):
    """
    Ask a worker about his capacities in all the abilities
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        abilities = Ability.objects.all()

        for ability in abilities:
            fname = 'ab_' + str(ability.id)

            self.fields[fname] = forms.FloatField()


class WorkersGenerationForm(forms.Form):
    """
    Form for user generation
    """
    num = forms.IntegerField(min_value=1, initial=1)

    method = forms.ChoiceField(choices=[
        ('zero', 'Zero'),
        ('one_spe', 'One speciality'),
        ('unif', 'Uniform')
    ])


class TreeForm(forms.Form):
    """
    Form for the tree generation
    """
    epsilon = forms.FloatField(initial=0.1)
    prof_max = forms.IntegerField(initial=7)
    bins = forms.IntegerField(initial=10)
    ratio = forms.FloatField(initial=0.7)
    coalition = forms.IntegerField(initial=1)


class TasksGenerationForm(forms.Form):
    """
    Form for tasks generation
    """
    num = forms.IntegerField(min_value=1, initial=1)

    method = forms.ChoiceField(choices=[
        ('random', 'Random'),
        ('pareto', 'Pareto'),
        ('one_spe', 'One speciality'),
        ('random_square_corner', 'Random square corner'),
        ('random_square', 'Random square'),
        ('random_square_3cases', 'Random square 3 cases')
    ])


class TaskHelperAPIForm(forms.Form):
    """
    Task creation helper for the API
    """
    def __init__(self, abilities, trees, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Build the abilities choices
        for ability in abilities:
            f = 'ab_' + str(ability.id)
            self.fields[f] = forms.FloatField()

        # Choosing the tree to work with
        self.fields['tree'] = forms.ChoiceField(choices=[
            (str(tree.id), tree) for tree in trees
        ])


class TaskHelperAPI_2Form(forms.Form):
    """
    Task creation helper for the API (v2)
    """
    def __init__(self, abilities, trees, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for ability in abilities:
            ab_min = 'ab_min_' + str(ability.id)
            ab_max = 'ab_max_' + str(ability.id)

            self.fields[ab_min] = forms.FloatField()
            self.fields[ab_max] = forms.FloatField()

        # Choosing the tree to work with
        self.fields['tree'] = forms.ChoiceField(choices=[
            (str(tree.id), tree) for tree in trees
        ])


class TaskForm(forms.Form):
    """
    New task creation form
    """
    title = forms.CharField(max_length=64, initial='')
    descr = forms.CharField(max_length=1024, initial='')

    def __init__(self, abilities, trees, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for ability in abilities:
            f_min = 'min_' + str(ability.id)
            f_max = 'max_' + str(ability.id)

            self.fields[f_min] = forms.FloatField()
            self.fields[f_max] = forms.FloatField()

        # Choosing the tree to work with
        self.fields['tree'] = forms.ChoiceField(choices=[
            (str(tree.id), tree) for tree in trees
        ])


class WorkersFileForm(forms.ModelForm):
    class Meta:
        model = WorkersCSVFile
        fields = ('file', )


class WorkersImportForm(forms.Form):

    def __init__(self, abilities, tags, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for ability in abilities:
            f = str(ability.id)
            self.fields[f] = forms.ChoiceField(choices=[
                (tag, tag) for tag in tags
            ])

    def is_valid(self, *args, **kwargs):
        valid = super().is_valid(*args, **kwargs)

        if valid:
            used_tags = []
            for key in self.cleaned_data:
                value = self.cleaned_data[key]
                if value in used_tags:
                    self.add_error(None, "A tag can only be used once")
                    return False
                used_tags += [value]
        return valid

    def make_link(self):
        link = {}
        for key in self.cleaned_data:
            value = self.cleaned_data[key]
            link[value] = key
        return link