# Crowdpriv demo

## Launch via docker-compose

Require docker and docker-compose

```
docker-compose build
docker-compose up
```

## Launch via podman

If your operating system is a Linux that uses cgroups V2 (e.g., Fedora 31) you 
can use podman and podman-compose rather than docker and docker-compose (that 
do not support cgroups V2 yet).   

```
sudo dnf install podman podman-compose
podman-compose build
podman-compose up
```

## Architecture & Documentation

django doc : https://docs.djangoproject.com/en/2.1/

- dossier crowdprivdemo : settings
- dossier crowdpriv : code
- Dockerfile : Docker container definition
- docker-compose.yml : dockers piloting file
- manage.py : django file, do not touch
- requirements.txt : Python package requirements
